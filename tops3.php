<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Lietotākās grāmatas</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Nosaukums</th>
                <th>Izdosanas_gads</th>
                <th>Sērijas kods</th>
                <th>Rezervāciju skaits</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('config.php');
            $sql = "SELECT * FROM rezervetakasgramatas";

            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $row["Nosaukums"] . '</td>';
                    echo '<td>' . $row["Izdosanas_gads"] . '</td>';
                    echo '<td>' . $row["SerijasKods"] . '</td>';
                    echo '<td>' . $row["RezervacijuSkaits"] . '</td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>