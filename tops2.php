<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Top10 rezervētākās sistēmas</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Nosaukums</th>
                <th>Izdosanas_gads</th>
                <th>Rezervāciju skaits</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('config.php');
            $sql = "SELECT * FROM top10rezervetasgramatas";

            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $row["Nosaukums"] . '</td>';
                    echo '<td>' . $row["Izdosanas_gads"] . '</td>';
                    echo '<td>' . $row["RezervacijuSkaits"] . '</td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>