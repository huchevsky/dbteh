<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
include('config.php');
?>
<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container col-sm-9 navbar-default" style="padding-top: 10px;">
    <legend>Meklēt Datoru</legend>
    <div class="row">
        <form class="form-horizontal" action="computer_result.php" method="get">
            <fieldset>
                <!-- Library -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Bibliotēka</label>
                    <div class="col-md-5">
                        <select class="selectpicker" name="library" data-live-search="true" title="Nosaukums Pilseta, Iela">
                            <?php
                            $sql = "SELECT * FROM biblioteka";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option value="' . $row["idBiblioteka"] . '" data-tokens="';
                                    echo $row["idBiblioteka"] . '">';
                                    echo $row["Nosaukums"] . ' ' . $row["Pilseta"] . ', ' . $row["Iela"];
                                    echo '</option>';
                                }
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Have printer -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="printer">Savienots ar printeri</label>
                    <div class="col-md-4">
                        <label class="radio-inline" for="printer-0">
                            <input type="radio" name="printer" id="printer-0" value="true">
                            Ir
                        </label>
                        <label class="radio-inline" for="printer-1">
                            <input type="radio" name="printer" id="printer-1" value="false" checked="checked">
                            Nav
                        </label>
                    </div>
                </div>

                <!-- Search Computer -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Meklēt</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
<?php
include('bottom.php');
?>