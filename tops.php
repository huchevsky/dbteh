<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
include('config.php');
?>
<div class="col-sm-9 navbar-default">
<div style="padding-top: 10px;">
    <legend>Informācija</legend>
    <!-- tabs left -->
    <div class="tabbable tabs-left">
        <ul class="nav nav-tabs">
            <li><a href="#a" data-toggle="tab">Top10 rezervētāji</a></li>
            <li class="active"><a href="#b" data-toggle="tab">Top10 rezervētākās grāmatas</a></li>
            <li><a href="#c" data-toggle="tab">Lietotākās grāmatas</a></li>
            <li><a href="#d" data-toggle="tab">Vispārīgā informācija</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="a">
                <?php
                    include('tops1.php');
                ?>
            </div>
            <div class="tab-pane active" id="b">
                <?php
                    include('tops2.php');
                ?>
            </div>
            <div class="tab-pane" id="c">
                <?php
                    include('tops3.php');
                ?>
            </div>
            <div class="tab-pane" id="d">
                <?php
                include('tops4.php');
                ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php
    include('bottom.php');
?>