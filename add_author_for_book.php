<?php
include('config.php');
?>
<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="add_author_for_book_action.php" method="post">
            <fieldset>
                <!-- Book -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Book">Grāmata</label>
                    <div class="col-md-5">
                        <select class="selectpicker" name="Book" data-live-search="true" title="Nosaukums">
                            <?php
                            $sql = "SELECT * FROM gramata";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option value="' . $row["idGramata"] . '" data-tokens="';
                                    echo $row["idGramata"] . '">';
                                    echo $row["Nosaukums"];
                                    echo '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Author -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Author">Autors</label>
                    <div class="col-md-5">
                        <select class="selectpicker" required="" name="Author" data-live-search="true" title="Vārds Uzvārds Dz. Gads">
                            <?php
                            $sql = "SELECT * FROM autors";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option  value="' . $row["idAutors"] . '" data-tokens="';
                                    echo $row["idAutors"] . '">';
                                    echo $row["Vards"] . ' ' . $row["Uzvards"] . ' ' . $row["Dzimsanas_gads"];
                                    echo '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Pievienot</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>