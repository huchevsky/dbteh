<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Datora rezervācijas</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Rezervacijas laiks</th>
                <th>Atdosanas laiks</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('config.php');
            $sql = "CALL lietotajadatorarezervacijas('". $_SESSION['persk'] . "')";

            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $row["Rezervacijas_laiks"] . '</td>';
                    echo '<td>' . $row["Atdosanas_laiks"] . '</td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>