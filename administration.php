<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
?>
<div class="col-sm-9 navbar-default">
<div style="padding-top: 10px;">
    <legend>Administrēšana</legend>
    <!-- tabs left -->
    <div class="tabbable tabs-left">
        <ul class="nav nav-tabs">
            <li><a href="#a" data-toggle="tab">Izveidot Lietotāju</a></li>
            <li class="active"><a href="#b" data-toggle="tab">Grāmatu izveidošana</a></li>
            <li><a href="#c" data-toggle="tab">Autoru Pievienošana</a></li>
            <li><a href="#d" data-toggle="tab">Datoru Pievienošana</a></li>
            <li><a href="#e" data-toggle="tab">Bibliotēkas Pievienošana</a></li>
            <li><a href="#f" data-toggle="tab">Žanra Pievienošana</a></li>
            <li><a href="#g" data-toggle="tab">Pievienot žanru grāmatai</a></li>
            <li><a href="#h" data-toggle="tab">Grāmatu pievienošana</a></li>
            <li><a href="#j" data-toggle="tab">Pievienot papildautoru</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="a">
                <?php
                    include('user_create.php');
                ?>
            </div>
            <div class="tab-pane active" id="b">
                <?php
                    include('book_create.php');
                ?>
            </div>
            <div class="tab-pane" id="c">
                <?php
                    include('author_create.php');
                ?>
            </div>
            <div class="tab-pane" id="d">
                <?php
                    include('computer_create.php');
                ?>
            </div>
            <div class="tab-pane" id="e">
                <?php
                include('library_create.php');
                ?>
            </div>
            <div class="tab-pane" id="f">
                <?php
                include('genre_create.php');
                ?>
            </div>
            <div class="tab-pane" id="g">
                <?php
                include('bookgenre_create.php');
                ?>
            </div>
            <div class="tab-pane" id="h">
                <?php
                include('bookpopulate_create.php');
                ?>
            </div>
            <div class="tab-pane" id="j">
                <?php
                include('add_author_for_book.php');
                ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php
    include('bottom.php');
?>