<div class="col-sm-2" style="margin-bottom: 50px;">
    <nav class="nav-sidebar navbar-default" style="-moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);">
        <ul class="nav">
            <script>
                function addActive(id) {
                    var el = document.getElementsByClassName("activemenu");
                    el[0].classList.remove("active");
                    el[0].classList.remove("activemenu");
                    document.getElementById(id).classList.add("active");
                    document.getElementById(id).classList.add("activemenu");
                }
            </script>
            <li id="menulist1" class="active activemenu"><a href="index.php" onclick="addActive('menulist1')">Sākums</a></li>
            <?php
            if(isset($_SESSION['persk']) || !empty($_SESSION['persk'])){
                echo '<li id="menulist2"><a href="book_search.php" onclick="addActive(\'menulist2\')">Grāmatas</a></li>';
                echo '<li id="menulist3"><a href="computer_search.php" onclick="addActive(\'menulist3\')">Datori</a></li>';
                if ($_SESSION['bib'] == 1) {
                    echo '<li id="menulist4"><a href="administration.php" onclick="addActive(\'menulist4\')">Administrācija</a></li>';
                }
                echo '<li id="menulist5"><a href="tops.php" onclick="addActive(\'menulist5\')">Papildus informācija</a></li>';
                echo '<li id="menulist6"><a href="profile.php" onclick="addActive(\'menulist6\')">Profils</a></li>';
                echo '<li id="menulist7"><a href="search_genre.php" onclick="addActive(\'menulist7\')">Žanri</a></li>';
            }
            ?>

        </ul>
    </nav>
</div>