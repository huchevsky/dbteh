<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" method="post" action="user_create_action.php">
            <fieldset>
                <!-- Name -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Vārds</label>
                    <div class="col-md-5">
                        <input id="Name" name="Name" type="text" placeholder="Vārds" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Surname">Uzvārds</label>
                    <div class="col-md-5">
                        <input id="Surname" name="Surname" type="text" placeholder="Uzvārds" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Personal code -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Perskods">Personas kods</label>
                    <div class="col-md-5">
                        <input id="Perskods" name="Perskods" type="text" placeholder="Personas kods"
                               class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Parole -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Passwordinput">Parole</label>
                    <div class="col-md-5">
                        <input id="Passwordinput" name="Passwordinput" type="password" placeholder="Parole"
                               class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Working for library -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="work">Bibliotēkas darbinieks</label>
                    <div class="col-md-4">
                        <label class="radio-inline" for="works-0">
                            <input type="radio" name="work" id="works-0" value="work">
                            Jā
                        </label>
                        <label class="radio-inline" for="works-1">
                            <input type="radio" name="work" id="works-1" value="work" checked="checked">
                            Nē
                        </label>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>