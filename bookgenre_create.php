<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="bookgenre_create_action.php" method="post">
            <fieldset>
                <!-- Book -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Book">Grāmata</label>
                    <div class="col-md-5">
                        <select class="selectpicker" name="Book" data-live-search="true" title="Grāmatas nosaukums">
                            <?php
                            $sql = "SELECT * FROM gramata";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option  value="' . $row["idGramata"] . '" data-tokens="';
                                    echo $row["idGramata"] . '">';
                                    echo $row["Nosaukums"] . ' ' . $row["Izdosanas_gads"];
                                    echo '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Genre -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Genre">Žanrs</label>
                    <div class="col-md-5">
                        <select class="selectpicker" name="Genre" data-live-search="true" title="Žanrs">
                            <?php
                            $sql = "SELECT * FROM gramataszanrs";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option  value="' . $row["idGramatasZanrs"] . '" data-tokens="';
                                    echo $row["idGramatasZanrs"] . '">';
                                    echo $row["Nosaukums"];
                                    echo '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>