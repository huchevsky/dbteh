<?php
include('config.php');
?>
<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="book_create_action.php" method="post">
            <fieldset>
                <!-- Name -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Nosaukums</label>
                    <div class="col-md-5">
                        <input id="Name" name="Name" type="text" placeholder="Nosaukums" class="form-control input-md"
                               required="">
                    </div>
                </div>

                <!-- Description -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Description">Apraksts</label>
                    <div class="col-md-5">
                        <input id="Description" name="Description" type="text" placeholder="Apraksts" class="form-control input-md"
                               required="">
                    </div>
                </div>

                <!-- Date -->
                <div class="form-group">
                    <label for="date" class="col-md-4 control-label">Izdošanas gads</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control" id="Date" name="Date">
                    </div>
                </div>

                <!-- Author -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Author">Autors</label>
                    <div class="col-md-5">
                        <select class="selectpicker" required="" name="Author" data-live-search="true" title="Vārds Uzvārds Dz. Gads">
                            <?php
                            $sql = "SELECT * FROM autors";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option  value="' . $row["idAutors"] . '" data-tokens="';
                                    echo $row["idAutors"] . '">';
                                    echo $row["Vards"] . ' ' . $row["Uzvards"] . ' ' . $row["Dzimsanas_gads"];
                                    echo '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>