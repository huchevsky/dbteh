<!-- Navigation -->
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <!-- Responsive web login details. -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">EasyBiblio</a>
        </div>

        <?php
            session_start();
            if(!isset($_SESSION['persk']) || empty($_SESSION['persk'])){
                include('login.php');
            } else {
                include('logout.php');
            }
        ?>

    </div>
</nav>