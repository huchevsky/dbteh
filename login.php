<?php
// Include config file
require_once('config.php');

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Check if username is empty
    if(!empty(trim($_POST["persk"]))){
        $username = trim($_POST["persk"]);
    }

    // Check if password is empty
    if(!empty(trim($_POST['parole']))){
        $password = trim($_POST['parole']);
    }

    // Validate credentials
    if(!empty($username) && !empty($password)){
        // Prepare a select statement
        $sql = "SELECT Personas_kods, Bibliotekars, Parole FROM lietotajs WHERE Personas_kods = '" . $username . "' AND Parole = '" . $password . "'";

        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
            session_start();
            $_SESSION['persk'] = $username;
            while($row = $result->fetch_assoc()) {
                $_SESSION['bib'] = $row['Bibliotekars'];
                $home_url = 'http://ovz.venta.lv/15.232/dbteh/index.php';
                header('Location:' . $home_url);
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>

<!-- Login details -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <form id="signin" class="navbar-form navbar-right" role="form"
          action="http://ovz.venta.lv/15.232/dbteh/index.php" method="post">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="persk" type="persk" class="form-control" name="persk" value="<?php echo $username; ?>"
                   placeholder="Personas kods">
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="parole" type="password" class="form-control" name="parole" value=""
                   placeholder="Parole">
        </div>

        <button type="submit" class="btn btn-primary">Pieslēgties</button>
    </form>
</div>