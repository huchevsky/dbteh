<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
include('config.php');
?>
    <!--https://bootsnipp.com/snippets/3XMOV-->
    <div class="container col-sm-9 navbar-default" style="padding-top: 10px;">
        <legend>Meklēt grāmatu</legend>
            <div class="row">
                <form class="form-horizontal" action="book_result.php" method="get">
                    <fieldset>
                        <!-- Name -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Name">Nosaukums</label>
                            <div class="col-md-5">
                                <input id="Name" name="Name" type="text" placeholder="Nosaukums"
                                       class="form-control input-md"
                                       required="">

                            </div>
                        </div>

                        <!-- Author -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Autors">Autors</label>
                            <div class="col-md-5">
                                <select class="selectpicker" name="Autors" data-live-search="true" title="Vārds Uzvārds Dz. Gads">
                                <?php
                                    $sql = "SELECT * FROM autors";
                                    $result = mysqli_query($conn, $sql);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while ($row = $result->fetch_assoc()) {

                                            echo '<option value="' . $row["idAutors"] . '" data-tokens="';
                                            echo $row["idAutors"] . '">';
                                            echo $row["Vards"] . ' ' . $row["Uzvards"] . ' ' . $row["Dzimsanas Gads"];
                                            echo '</option>';
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div>

                        <!-- Library -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Name">Bibliotēka</label>
                            <div class="col-md-5">
                                <select class="selectpicker" name="Biblio" data-live-search="true" title="Nosaukums Pilseta, Iela">
                                    <?php
                                    $sql = "SELECT * FROM biblioteka";
                                    $result = mysqli_query($conn, $sql);
                                    if ($result->num_rows > 0) {
                                        // output data of each row
                                        while($row = $result->fetch_assoc()) {

                                            echo '<option value="' . $row["idBiblioteka"] . '"data-tokens="';
                                            echo $row["idBiblioteka"] . '">';
                                            echo $row["Nosaukums"] . ' ' . $row["Pilseta"] . ', ' . $row["Iela"];
                                            echo '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Search book -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="submit"></label>
                            <div class="col-md-4">
                                <button id="submit" name="submit" class="btn btn-success">Meklēt</button>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
    </div>
<?php
include('bottom.php');
?>