<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Top10 grāmatu rezervētāji</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Vārds</th>
                <th>Uzvārds</th>
                <th>Rezervāciju skaits</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('config.php');
            $sql = "SELECT * FROM top10gramaturezervetaji";

            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $row["Vards"] . '</td>';
                    echo '<td>' . $row["Uzvards"] . '</td>';
                    echo '<td>' . $row["RezervacijuSkaits"] . '</td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>