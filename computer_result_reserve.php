<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
?>

<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Datoru rezervacija</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Laiks</th>
                <th>Rezervācija</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once 'config.php';
            session_start();
            $selectedTime = "8:00:00";
            $selectedTime2 = "9:00:00";
            for ($x = 0; $x <= 8; $x++) {
                $endTime = strtotime("+". $x . " hour", strtotime($selectedTime));
                $endTime2 = strtotime("+". $x . " hour", strtotime($selectedTime2));

                echo '<tr>';
                echo '<td>' . date('H:i:s', $endTime) . ' - '. date('H:i:s', $endTime2) .  '</td>';

                $sql = "SELECT * FROM datorarezervacija JOIN rezervacija on rezervacija.idRezervacija = datorarezervacija.idRezervacija
                WHERE datorarezervacija.idDators = " . $_GET['id'] . " AND Rezervacijas_laiks = '" . date('Y-m-d H:i:s', $endTime) . "'";
                $result = mysqli_query($conn, $sql);
                if ($result->num_rows > 0) {
                    echo '<td>Aizņemts</td>';
                } else {
                    echo '<td><a href="computer_reserve.php?id='. $x .'&comp=' . $_GET['id'] . '&date='
                        . date('Y-m-d H:i:s', $endTime) . '&date2=' . date('Y-m-d H:i:s', $endTime2) .'" class="btn btn-primary">Rezervēt</a></td>';
                }
                echo '</tr>';
            }

            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>
<?php
include('bottom.php');
?>