<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
include('config.php');
?>
<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container col-sm-9 navbar-default" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="search_genre_action.php" method="post">
            <fieldset>
                <!-- Genre -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Book">Žanrs</label>
                    <div class="col-md-5">
                        <select class="selectpicker" name="genre" data-live-search="true" title="Žanrs">
                            <?php
                            $sql = "SELECT * FROM gramataszanrs";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option value="' . $row["Nosaukums"] . '" data-tokens="';
                                    echo $row["Nosaukums"] . '">';
                                    echo $row["Nosaukums"];
                                    echo '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Meklēt</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<?php
include('bottom.php');
?>
