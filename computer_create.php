<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="computer_create_action.php" method="post">
            <fieldset>
                <!-- Has Printer -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="printer">Savienots ar printeri</label>
                    <div class="col-md-4">
                        <label class="radio-inline" for="printer-0">
                            <input type="radio" name="printer" id="printer-0" value="1">
                            Jā
                        </label>
                        <label class="radio-inline" for="printer-1">
                            <input type="radio" name="printer" id="printer-1" value="0" checked="checked">
                            Nē
                        </label>
                    </div>
                </div>

                <!-- Library -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Bibliotēka</label>
                    <div class="col-md-5">
                        <select class="selectpicker" name="library" data-live-search="true" title="Nosaukums Pilseta, Iela">
                            <?php
                            include_once 'config.php';
                            $sql = "SELECT * FROM biblioteka";
                            $result = mysqli_query($conn, $sql);
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {

                                    echo '<option value="' . $row["idBiblioteka"] . '" data-tokens="';
                                    echo $row["idBiblioteka"] . '">';
                                    echo $row["Nosaukums"] . ' ' . $row["Pilseta"] . ', ' . $row["Iela"];
                                    echo '</option>';
                                }
                            } else {
                                echo "0 results";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>