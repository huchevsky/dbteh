<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="library_create_action.php" method="post">
            <fieldset>
                <!-- Name -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Nosaukums</label>
                    <div class="col-md-5">
                        <input id="Name" name="Name" type="text" placeholder="Nosaukums" class="form-control input-md"
                               required="">
                    </div>
                </div>

                <!-- Street -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Street">Iela</label>
                    <div class="col-md-5">
                        <input id="Street" name="Street" type="text" placeholder="Iela" class="form-control input-md"
                               required="">
                    </div>
                </div>

                <!-- City -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="City">Pilsēta</label>
                    <div class="col-md-5">
                        <input id="City" name="City" type="text" placeholder="Pilsēta" class="form-control input-md"
                               required="">
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>