<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
?>
    <div class="col-sm-9 navbar-default">
        <div style="padding-top: 10px;">
            <legend>Profila informācija</legend>
            <!-- tabs left -->
            <div class="tabbable tabs-left">
                <ul class="nav nav-tabs">
                    <li><a href="#a" data-toggle="tab">Datoru rezervācijas</a></li>
                    <li class="active"><a href="#b" data-toggle="tab">Grāmatu rezervācijas</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="a">
                        <?php
                        include('profile1.php');
                        ?>
                    </div>
                    <div class="tab-pane active" id="b">
                        <?php
                        include('profile2.php');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include('bottom.php');
?>