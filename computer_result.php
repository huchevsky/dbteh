<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
?>

<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Datoru rezervacija</legend>
    <div class="row">
            <table class="table" id="table">
                <thead>
                <tr>
                    <th>Printeris</th>
                    <th>Bibliotēka</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    require_once 'config.php';
                    $sqlLib = ' JOIN biblioteka on biblioteka.idBiblioteka = dators.idBiblioteka';
                    $sqlLib2 = '';
                    if (!empty($_GET['library'])) {
                        $sqlLib2 = ' AND dators.idBiblioteka = ' . $_GET['library'];
                    }

                    $sql = "SELECT * FROM dators" . $sqlLib . " WHERE irPrinteris = " . $_GET['printer'] . $sqlLib2;
                    //echo $sql;
                    $result = mysqli_query($conn, $sql);
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            echo '<tr>';
                                if ($row["IrPrinteris"] == 1) {
                                    echo '<td><span class="glyphicon glyphicon-ok"></span></td>';
                                } else {
                                    echo '<td><span class="glyphicon glyphicon-remove"></span></td>';
                                }
                                echo '<td>' . $row["Nosaukums"] . " " . $row["Pilseta"] . ", " . $row["Iela"] . '</td>';
                                echo '<td><a href="computer_result_reserve.php?id='. $row["idDators"] .'" class="btn btn-primary">Rezervēt</a></td>';
                            echo '</tr>';
                        }
                    }
                ?>
                </tbody>
            </table>
            <hr>
    </div>
</div>
<?php
include('bottom.php');
?>