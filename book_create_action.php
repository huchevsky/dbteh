<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
include('config.php');
?>
<div class="container col-sm-9 navbar-default" style="padding-top: 10px;">
    <legend>Grāmatas izveidošana</legend>
    <div class="row">
        <?php
        $sql = "INSERT INTO `gramata` (`idGramata`, `Nosaukums`, `Apraksts`, `Izdosanas_gads`)" .
            " VALUES (NULL, '" . $_POST["Name"] . "', '". $_POST["Description"] . "', '". $_POST["Date"] . "')";
        if ($conn->query($sql) === TRUE) {
            $last_id = $conn->insert_id;
            $sql2 = "INSERT INTO `gramatasautors` (`idGramata`, `idAutors`) VALUES ('" . $last_id . "', '" . $_POST["Author"] . "')";
            $conn->query($sql2);
            echo "<p>Jauna gramata izveidota.</p>";
        } else {
            echo "<p>Radās kļūda veidojot gramatu.</p>";
        }
        ?>
    </div>
</div>
<?php
include('bottom.php');
?>
