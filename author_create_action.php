<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
include('config.php');
?>
<div class="container col-sm-9 navbar-default" style="padding-top: 10px;">
    <legend>Autoru izveidošana</legend>
    <div class="row">
        <?php
        $sql = "INSERT INTO `autors` (`idAutors`, `Vards`, `Uzvards`, `Dzimsanas_gads`)" .
                " VALUES (NULL, '" . $_POST["Name"] . "', '". $_POST["Surname"] . "', '". $_POST["Date"] . "')";
        if ($conn->query($sql) === TRUE) {
            echo "<p>Jauns Autors izveidots.</p>";
        } else {
            echo "<p>Radās kļūda veidojot autoru.</p>";
        }
        ?>
    </div>
</div>
<?php
    include('bottom.php');
?>
