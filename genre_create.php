<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="genre_create_action.php" method="post">
            <fieldset>
                <!-- Name -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Žanrs</label>
                    <div class="col-md-5">
                        <input id="Name" name="Name" type="text" placeholder="Nosaukums" class="form-control input-md"
                               required="">
                    </div>
                </div>

                <!-- Description -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Description">Žanra apraksts</label>
                    <div class="col-md-5">
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>