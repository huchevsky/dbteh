<!--https://bootsnipp.com/snippets/3XMOV-->
<div class="container" style="padding-top: 10px;">
    <div class="row">
        <form class="form-horizontal" action="author_create_action.php" method="post">
            <fieldset>
                <!-- Name -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Vārds</label>
                    <div class="col-md-5">
                        <input id="Name" name="Name" type="text" placeholder="Vārds" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Surname">Uzvārds</label>
                    <div class="col-md-5">
                        <input id="Surname" name="Surname" type="text" placeholder="Uzvārds" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Date -->
                <div class="form-group">
                    <label for="date" class="col-md-4 control-label">Dzimšanas gads</label>
                    <div class="col-sm-5">
                        <input type="date" format="YYYY-MM-DD" class="form-control" id="Date" name="Date">
                    </div>
                </div>

                <!-- Registration complete -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Reģistrēt</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>