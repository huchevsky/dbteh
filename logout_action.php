<?php
    require_once('config.php');

    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        // Initialize the session
        session_start();

        // Unset all of the session variables
        $_SESSION = array();

        // Destroy the session.
        session_destroy();

        $home_url = 'http://ovz.venta.lv/15.232/dbteh/index.php';
        header('Location:' . $home_url);
    }
?>