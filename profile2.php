<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Grāmatu rezervācijas</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Grāmatas nosaukums</th>
                <th>Rezervacijas laiks</th>
                <th>Atdosanas laiks</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require('config.php');
            $sql = "CALL lietotajagramaturezervacijas('". $_SESSION['persk'] . "')";

            //echo $sql;

            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $row["Nosaukums"] . '</td>';
                    echo '<td>' . $row["Rezervacijas_laiks"] . '</td>';
                    echo '<td>' . $row["Atdosanas_laiks"] . '</td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>