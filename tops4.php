<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Vispārīgā informācija</legend>
    <div class="row">
        <?php
        require_once('config.php');
        $sql = "SELECT rezervejumuskaists() AS rez";

        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                echo '<p>Kopējais rezervējumu skaits sistēmā: ' . $row["rez"] . '</p>';
            }
        }

        $sql = "SELECT vecakagramata() AS vec";

        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                echo '<p>Vecākās grāmatas izdošanas gads: ' . $row["vec"] . '</p>';
            }
        }
        ?>
        <hr>
    </div>
</div>