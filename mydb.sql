-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2017 at 01:38 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `gramataspeczanra` (IN `zanrs` VARCHAR(255))  begin    
SELECT gramata.Nosaukums FROM gramata JOIN gramataszanri ON gramataszanri.idGramata = gramata.idGramata 
JOIN gramataszanrs ON gramataszanrs.idGramatasZanrs = gramataszanri.idGramatasZanrs WHERE gramataszanrs.Nosaukums LIKE CONCAT("%",zanrs,"%");
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `lietotajadatorarezervacijas` (IN `perskods` VARCHAR(255))  BEGIN    
SELECT lietotajs.Vards, lietotajs.Uzvards, rezervacija.Rezervacijas_laiks, rezervacija.Atdosanas_laiks FROM datorarezervacija
JOIN rezervacija ON rezervacija.idRezervacija = datorarezervacija.idRezervacija 
 JOIN lietotajs ON lietotajs.idLietotajs = rezervacija.Lietotajs_idLietotajs WHERE lietotajs.Personas_kods = perskods;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `lietotajagramaturezervacijas` (IN `perskods` VARCHAR(255))  BEGIN    
SELECT gramata.Nosaukums, lietotajs.Vards, lietotajs.Uzvards, rezervacija.Rezervacijas_laiks, 	rezervacija.Atdosanas_laiks 
FROM gramatasrezervacija JOIN rezervacija ON rezervacija.idRezervacija = 	gramatasrezervacija.idRezervacija 
JOIN lietotajs ON lietotajs.idLietotajs = rezervacija.Lietotajs_idLietotajs
JOIN gramatas ON gramatas.SerijasKods = gramatasrezervacija.SerijasKods
JOIN gramata ON gramata.idGramata = gramatas.idGramata
WHERE lietotajs.Personas_kods = perskods;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `bibliotekars` (`personaskods` VARCHAR(255)) RETURNS INT(11) BEGIN       
		DECLARE bib INT;  
 
		SELECT Bibliotekars INTO bib FROM lietotajs WHERE Personas_kods = personaskods;  
        RETURN bib;
	END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `rezervejumuskaists` () RETURNS INT(11) BEGIN       
	DECLARE skaits INT;  
 
	SELECT Count(idRezervacija) INTO skaits FROM rezervacija;  
	RETURN skaits;    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `vecakagramata` () RETURNS DATETIME BEGIN       
		DECLARE gads DATETIME;  
 
		SELECT MIN(Izdosanas_gads) INTO gads FROM Gramata;  
		RETURN gads;    
	END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `autors`
--

CREATE TABLE `autors` (
  `idAutors` int(11) NOT NULL,
  `Vards` varchar(45) DEFAULT NULL,
  `Uzvards` varchar(45) DEFAULT NULL,
  `Dzimsanas_gads` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `autors`
--

INSERT INTO `autors` (`idAutors`, `Vards`, `Uzvards`, `Dzimsanas_gads`) VALUES
(1, 'Rūdolfs', 'Blaumanis', '1863-01-01'),
(2, 'Regīna', 'Ezera', '1930-12-20'),
(3, 'Nora', 'Ikstena', '1969-10-15'),
(4, 'Jānis', 'Jaunsudrabiņš', '1877-08-25'),
(6, 'Imants', 'Ziedonis', '1933-03-03'),
(7, 'Normunds', 'Kuzmičovs', '1995-03-06'),
(8, 'Testa', 'Autors', '1970-03-03');

-- --------------------------------------------------------

--
-- Table structure for table `biblioteka`
--

CREATE TABLE `biblioteka` (
  `idBiblioteka` int(11) NOT NULL,
  `Nosaukums` varchar(45) DEFAULT NULL,
  `Iela` varchar(45) DEFAULT NULL,
  `Pilseta` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `biblioteka`
--

INSERT INTO `biblioteka` (`idBiblioteka`, `Nosaukums`, `Iela`, `Pilseta`) VALUES
(1, 'Rīgas Centrālā bibliotēka', 'Rīga', 'Brīvības iela 49/53'),
(2, 'Kuldīgas Galvenā bibliotēka', 'Kuldīga', '1905. gada iela 6'),
(3, 'Liepājas Centrālā zinātniskā bibliotēka', 'Liepāja', ' Zivju 7'),
(4, 'Ventspils bibliotēka', 'Ventspils', 'Akmeņu iela 2'),
(5, 'Dobeles novada centrālā bibliotēka', 'Dobele', 'Brīvības 23'),
(6, 'Latgales centrālā bibliotēka', 'Daugavpils', 'Rīgas iela 22');

-- --------------------------------------------------------

--
-- Table structure for table `datorarezervacija`
--

CREATE TABLE `datorarezervacija` (
  `idRezervacija` int(11) NOT NULL,
  `idDators` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `datorarezervacija`
--

INSERT INTO `datorarezervacija` (`idRezervacija`, `idDators`) VALUES
(6, 10),
(7, 1),
(8, 1),
(9, 10),
(17, 3),
(18, 4),
(20, 1),
(21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dators`
--

CREATE TABLE `dators` (
  `idDators` int(11) NOT NULL,
  `IrPrinteris` tinyint(1) DEFAULT NULL,
  `idBiblioteka` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dators`
--

INSERT INTO `dators` (`idDators`, `IrPrinteris`, `idBiblioteka`) VALUES
(1, 1, 2),
(2, 1, 2),
(3, 0, 2),
(4, 1, 1),
(5, 0, 1),
(6, 1, 1),
(7, 1, 5),
(8, 1, 4),
(9, 1, 6),
(10, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `gramata`
--

CREATE TABLE `gramata` (
  `idGramata` int(11) NOT NULL,
  `Nosaukums` varchar(100) DEFAULT NULL,
  `Apraksts` varchar(1000) DEFAULT NULL,
  `Izdosanas_gads` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gramata`
--

INSERT INTO `gramata` (`idGramata`, `Nosaukums`, `Apraksts`, `Izdosanas_gads`) VALUES
(1, 'Raudupiete', 'Raudupietes tēls latviešu literatūrā ir pirmais, kurā ir parādīta cilvēka iekšējās pasaules ļaunā puse. Raudupiete apprecēja vecāku vīru, tātad viņa nepaspēja izbaudīt jaunības mīlestību. Viņai ir iespēja saņemt šo mīlestību no Kārļa. Šī iespēja viņai šķiet labāka un svarīgāka par miesīgo dēlu. Raudupieti moka smagi iekšējie pārdzīvojumi, kad viņai ir jāizšķiras starp mīlestību un dēlu. Šeit arī izpaužas viņas ļaunā puse, jo viņa savas iegribas dēļ nodod savu bērnu...', '1889-01-01'),
(2, 'Slazds', 'Nav apraksts', '1979-01-01'),
(3, 'Vīrs zilajā lietusmētelītī', 'Vīrs zilajā lietusmētelītī ir Dzintars Sodums, kurš grāmatā maskējas ar vārdu Tebe (centrālā persona Soduma autobiogrāfiskajos darbos) un ar kuru jaunā rakstniece Meitiņa (Ikstenas vārds grāmatā nav pieminēts) epistolārā veidā sapazinās jau deviņdesmito gadu vidū. Sekoja sarakste vairāku gadu ilgumā, iepazīšanās klātienē, līdz beidzot Ikstena Sodumu atgādāja uz Latviju, izmitināja pie sevis, un dzīves divus pēdējos gadus, kurus viņš pats raksturoja kā laimīgākos savā mūžā, Sodums pavadīja dzimtenē.', '2011-01-01'),
(4, 'Nauda', 'Nav apraksts', '1942-01-01'),
(5, 'Dzīves svinēšana', 'Dzīves svinēšana ir ne vien grāmatas, bet arī žanra nosaukums.” Ja īsi jāatbild uz tradicionālo jautājumu, par ko ir Dzīves svinēšana, jāsaka – par bērēm, ar šo atbildi riskējot atbaidīt potenciālo lasītāju, kuram līdz šim nav izveidojies savs priekšstats par Ikstenas prozu. Kā gan iespējams uzrakstīt veselu romānu par bērēm, turklāt tā, lai lasāmgabals nebūtu kārtējā gaudulīgā ziņģēšana vai apnāves prātniecisko formulu atgremošana? Izrādās, ir iespējams (un to apzinās ne jau Nora Ikstena vien – piemēram, pirms pāris gadiem Anglijā Booker prēmiju ieguva Greiama Svifta romāns Pēdējie rīkojumi, kurā aprakstīts garumgarš bērēšanas brauciens, bet, meklējot pavisam klasiskus priekšgājējus, atmiņā nāk, protams, Džoisa Finegana vāķis).', '1998-01-01'),
(6, 'Testa grāmata', 'Grāmata izveidota, lai testētu sistēmu', '2017-11-25');

--
-- Triggers `gramata`
--
DELIMITER $$
CREATE TRIGGER `pec_gramatas_izdzesanas` BEFORE DELETE ON `gramata` FOR EACH ROW BEGIN   
	DELETE FROM gramataszanri WHERE gramataszanri.idGramata=gramata.idGramata; 
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `pec_gramatas_izdzesanas2` AFTER DELETE ON `gramata` FOR EACH ROW BEGIN   
	DELETE FROM gramatasautors WHERE gramatasautors.idGramata=gramata.idGramata; 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `gramatas`
--

CREATE TABLE `gramatas` (
  `SerijasKods` int(11) NOT NULL,
  `idGramata` int(11) NOT NULL,
  `idBiblioteka` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gramatas`
--

INSERT INTO `gramatas` (`SerijasKods`, `idGramata`, `idBiblioteka`) VALUES
(1, 1, 4),
(2, 3, 1),
(3, 4, 6),
(4, 1, 2),
(5, 5, 4),
(6, 2, 5),
(7, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `gramatasautors`
--

CREATE TABLE `gramatasautors` (
  `idGramata` int(11) NOT NULL,
  `idAutors` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gramatasautors`
--

INSERT INTO `gramatasautors` (`idGramata`, `idAutors`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 3),
(6, 7),
(6, 8);

-- --------------------------------------------------------

--
-- Table structure for table `gramatasrezervacija`
--

CREATE TABLE `gramatasrezervacija` (
  `idRezervacija` int(11) NOT NULL,
  `SerijasKods` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gramatasrezervacija`
--

INSERT INTO `gramatasrezervacija` (`idRezervacija`, `SerijasKods`) VALUES
(12, 1),
(13, 3),
(14, 5),
(15, 2),
(16, 4),
(19, 7);

-- --------------------------------------------------------

--
-- Table structure for table `gramataszanri`
--

CREATE TABLE `gramataszanri` (
  `idGramatasZanrs` int(11) NOT NULL,
  `idGramata` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gramataszanri`
--

INSERT INTO `gramataszanri` (`idGramatasZanrs`, `idGramata`) VALUES
(1, 1),
(2, 3),
(2, 4),
(2, 5),
(6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `gramataszanrs`
--

CREATE TABLE `gramataszanrs` (
  `idGramatasZanrs` int(11) NOT NULL,
  `Nosaukums` varchar(100) DEFAULT NULL,
  `Apraksts` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gramataszanrs`
--

INSERT INTO `gramataszanrs` (`idGramatasZanrs`, `Nosaukums`, `Apraksts`) VALUES
(1, 'Novele', 'Novele ir neliela apjoma prozas sacerējums, kura pamatā ir raksturīgs spraigs sižeta risinājums ar neparastu komisku vai traģisku notikumu centrā, bieži ar negaidītu atrisinājumu.[1] Neparasts, bet reāli iedomājams, notikums, kas tiek risināts spraigi un strauji, nenovirzoties, ar izvērstām atkāpēm no galvenās sižeta līnijas. Darbības norisi, kas balstās uz centrālo konfliktu, būtiski raksturo kulminācijas un pavērsiena punkti un necerēti/negaidīts atrisinājums. Noveles pamatā ir dramatisks notikums ar negaidītu atrisinājumu. Spilgts notikums, kas tiek risināts spraigi un strauji, īsā laika periodā; darbības norisi būtiski raksturo kulminācijas un pavērsiena punkti un negaidīts atrisinājums.'),
(2, 'Romāns', 'Romāns ir visplašākais prozas darbs vai sacerējums, kurā parasti raksturīgi izvērsta tēlu sistēma, atklāta atsevišķa cilvēka un sabiedrības attieksme ilgākā laika posmā, daudzpusīgi atsedzot galvenā varoņa dzīvi, personību, izaugsmi, domu un jūtu pasauli. Sarežģīta kompozīcija, plašs, detalizēts vēstījums par tēloto laiku, galveno varoņu dzīvi un viņu attiecībām ar sava laikmeta sabiedrību. Izdomātu personu un notikumu tēlojums vispārinātā pasaules un cilvēcisko attieksmju skatījumā.'),
(3, 'Stāsts', 'Stāsts ir prozas darbs vai sacerējums, žanrisks apzīmējums visiem tiem literāriem darbiem, kurā atainota un tēlota viena vai dažādu cilvēku rīcība ir neliels posms vairākos notikumos galvenā tēla darbības rīcībā un dzīvē. Pārējās personas tiek raksturotas tikai attieksmē pret centrālo tēlu.'),
(4, 'Biogrāfija', 'Biogrāfija (no franču: biographie; sengrieķu bios ‘dzīve’ + graphein ‘rakstīt’) ir literatūras vai kādas citas mākslas formas (piemēram, filmas) žanrs, kurā ir kāda cilvēka dzīves hronoloģisks apraksts. Tajā vairāk tiek aprakstīti fakti nekā notikumi. Atšķirībā no cilvēka profila un CV biogrāfijas rakstos vairāk tiek skarta cilvēka personība, pieredze un individuālisms. Biogrāfijā tiek rakstīti svarīgi fakti, kā dzimšanas dati, izglītība, darbs, sasniegumi, attiecības ar citiem cilvēkiem, atrašanās sabiedrībā un nāve.'),
(5, 'Humoreska', 'Humoreska ir neliels prozas sacerējums, kurā izpaužas labsirdīgi zobgalīga attieksme pret cilvēka rakstura īpašībām vai dzīves parādībām. Humoreska var būt arī humoristisks muzikāls darbs.'),
(6, 'Proza', 'Proza (latīņu: prosa — ‘tiešs’) jeb epika (grieķu: epikos — ‘vēstījošs’) ir daiļliteratūras veids, kas rakstīts nesaistītā valodā. Prozai tradicionāli bijis raksturīgs sižetiskums un lielākoties daudz mazāka tēlainība nekā dzejai, taču kopš 20. gadsimta sākuma modernisma, avangarda un vēlāk arī postmodernisma literatūrā bieži vien sastopami, piemēram, darbi bez sižeta un ļoti tēlaini prozas darbi (aizsākumi šādai prozai meklējami jau impresionisma literatūrā).');

-- --------------------------------------------------------

--
-- Table structure for table `lietotajs`
--

CREATE TABLE `lietotajs` (
  `idLietotajs` int(11) NOT NULL,
  `Vards` varchar(45) DEFAULT NULL,
  `Uzvards` varchar(45) DEFAULT NULL,
  `Personas_kods` varchar(45) NOT NULL,
  `Bibliotekars` tinyint(1) DEFAULT NULL,
  `Parole` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lietotajs`
--

INSERT INTO `lietotajs` (`idLietotajs`, `Vards`, `Uzvards`, `Personas_kods`, `Bibliotekars`, `Parole`) VALUES
(1, 'Normunds', 'Kuzmicovs', '030303-12345', 1, 'parole'),
(2, 'Janis', 'Zarins', '060606-12345', 0, 'parole'),
(3, 'Laura', 'Upe', '030405-12345', 0, 'parole'),
(4, 'Karlīna', 'Mieze', '121275-12345', 0, 'parole'),
(5, 'Dzintra', 'Bērza', '010155-212121', 1, 'parole');

-- --------------------------------------------------------

--
-- Table structure for table `rezervacija`
--

CREATE TABLE `rezervacija` (
  `idRezervacija` int(11) NOT NULL,
  `Rezervacijas_laiks` datetime DEFAULT NULL,
  `Atdosanas_laiks` datetime DEFAULT NULL,
  `Lietotajs_idLietotajs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rezervacija`
--

INSERT INTO `rezervacija` (`idRezervacija`, `Rezervacijas_laiks`, `Atdosanas_laiks`, `Lietotajs_idLietotajs`) VALUES
(6, '2017-11-21 08:00:00', '2017-11-21 09:00:00', 1),
(7, '2017-11-21 10:00:00', '2017-11-21 11:00:00', 1),
(8, '2017-11-21 09:00:00', '2017-11-21 10:00:00', 1),
(9, '2017-11-21 08:00:00', '2017-11-21 09:00:00', 1),
(12, '2017-11-21 19:49:59', '2017-12-05 19:49:59', 1),
(13, '2017-11-21 19:50:34', '2017-12-05 19:50:34', 1),
(14, '2017-11-21 19:55:54', '2017-12-05 19:55:54', 1),
(15, '2017-11-21 19:56:05', '2017-12-05 19:56:05', 1),
(16, '2017-11-21 19:56:17', '2017-12-05 19:56:17', 1),
(17, '2017-11-21 11:00:00', '2017-11-21 12:00:00', 1),
(18, '2017-11-21 09:00:00', '2017-11-21 10:00:00', 1),
(19, '2017-11-22 22:17:07', '2017-12-06 22:17:07', 2),
(20, '2017-11-22 09:00:00', '2017-11-22 10:00:00', 2),
(21, '2017-11-22 09:00:00', '2017-11-22 10:00:00', 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `rezervetakasgramatas`
--
CREATE TABLE `rezervetakasgramatas` (
`Nosaukums` varchar(100)
,`Izdosanas_gads` date
,`SerijasKods` int(11)
,`RezervacijuSkaits` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `top10gramaturezervetaji`
--
CREATE TABLE `top10gramaturezervetaji` (
`Vards` varchar(45)
,`Uzvards` varchar(45)
,`RezervacijuSkaits` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `top10rezervetasgramatas`
--
CREATE TABLE `top10rezervetasgramatas` (
`Nosaukums` varchar(100)
,`Izdosanas_gads` date
,`RezervacijuSkaits` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `rezervetakasgramatas`
--
DROP TABLE IF EXISTS `rezervetakasgramatas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rezervetakasgramatas`  AS  select `gramata`.`Nosaukums` AS `Nosaukums`,`gramata`.`Izdosanas_gads` AS `Izdosanas_gads`,`gramatas`.`SerijasKods` AS `SerijasKods`,count(`gramatasrezervacija`.`idRezervacija`) AS `RezervacijuSkaits` from ((`gramatasrezervacija` join `gramatas` on((`gramatas`.`SerijasKods` = `gramatasrezervacija`.`SerijasKods`))) join `gramata` on((`gramata`.`idGramata` = `gramatas`.`idGramata`))) group by `gramatas`.`SerijasKods` ;

-- --------------------------------------------------------

--
-- Structure for view `top10gramaturezervetaji`
--
DROP TABLE IF EXISTS `top10gramaturezervetaji`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `top10gramaturezervetaji`  AS  select `lietotajs`.`Vards` AS `Vards`,`lietotajs`.`Uzvards` AS `Uzvards`,count(`gramatasrezervacija`.`SerijasKods`) AS `RezervacijuSkaits` from ((`rezervacija` join `gramatasrezervacija` on((`gramatasrezervacija`.`idRezervacija` = `rezervacija`.`idRezervacija`))) join `lietotajs` on((`lietotajs`.`idLietotajs` = `rezervacija`.`Lietotajs_idLietotajs`))) group by `rezervacija`.`Lietotajs_idLietotajs` order by count(`gramatasrezervacija`.`SerijasKods`) desc limit 10 ;

-- --------------------------------------------------------

--
-- Structure for view `top10rezervetasgramatas`
--
DROP TABLE IF EXISTS `top10rezervetasgramatas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `top10rezervetasgramatas`  AS  select `gramata`.`Nosaukums` AS `Nosaukums`,`gramata`.`Izdosanas_gads` AS `Izdosanas_gads`,count(`gramatasrezervacija`.`idRezervacija`) AS `RezervacijuSkaits` from ((`gramatasrezervacija` join `gramatas` on((`gramatas`.`SerijasKods` = `gramatasrezervacija`.`SerijasKods`))) join `gramata` on((`gramata`.`idGramata` = `gramatas`.`idGramata`))) group by `gramata`.`idGramata` limit 10 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autors`
--
ALTER TABLE `autors`
  ADD PRIMARY KEY (`idAutors`);

--
-- Indexes for table `biblioteka`
--
ALTER TABLE `biblioteka`
  ADD PRIMARY KEY (`idBiblioteka`);

--
-- Indexes for table `datorarezervacija`
--
ALTER TABLE `datorarezervacija`
  ADD PRIMARY KEY (`idRezervacija`,`idDators`),
  ADD KEY `fk_DatoraRezervacija_Dators1_idx` (`idDators`);

--
-- Indexes for table `dators`
--
ALTER TABLE `dators`
  ADD PRIMARY KEY (`idDators`),
  ADD KEY `fk_Dators_Biblioteka1_idx` (`idBiblioteka`);

--
-- Indexes for table `gramata`
--
ALTER TABLE `gramata`
  ADD PRIMARY KEY (`idGramata`);

--
-- Indexes for table `gramatas`
--
ALTER TABLE `gramatas`
  ADD PRIMARY KEY (`SerijasKods`,`idGramata`),
  ADD KEY `fk_Gramatas_Gramata1_idx` (`idGramata`);

--
-- Indexes for table `gramatasautors`
--
ALTER TABLE `gramatasautors`
  ADD PRIMARY KEY (`idGramata`,`idAutors`),
  ADD KEY `fk_GramatasAutors_Autors1_idx` (`idAutors`);

--
-- Indexes for table `gramatasrezervacija`
--
ALTER TABLE `gramatasrezervacija`
  ADD PRIMARY KEY (`idRezervacija`,`SerijasKods`),
  ADD KEY `fk_GramatasRezervacija_Gramatas1_idx` (`SerijasKods`);

--
-- Indexes for table `gramataszanri`
--
ALTER TABLE `gramataszanri`
  ADD PRIMARY KEY (`idGramatasZanrs`,`idGramata`),
  ADD KEY `fk_GramatasZanri_GramatasZanrs_idx` (`idGramatasZanrs`),
  ADD KEY `fk_GramatasZanri_Gramata1_idx` (`idGramata`);

--
-- Indexes for table `gramataszanrs`
--
ALTER TABLE `gramataszanrs`
  ADD PRIMARY KEY (`idGramatasZanrs`);

--
-- Indexes for table `lietotajs`
--
ALTER TABLE `lietotajs`
  ADD PRIMARY KEY (`idLietotajs`);

--
-- Indexes for table `rezervacija`
--
ALTER TABLE `rezervacija`
  ADD PRIMARY KEY (`idRezervacija`,`Lietotajs_idLietotajs`),
  ADD KEY `fk_Rezervacija_Lietotajs1_idx` (`Lietotajs_idLietotajs`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autors`
--
ALTER TABLE `autors`
  MODIFY `idAutors` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `biblioteka`
--
ALTER TABLE `biblioteka`
  MODIFY `idBiblioteka` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dators`
--
ALTER TABLE `dators`
  MODIFY `idDators` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `gramata`
--
ALTER TABLE `gramata`
  MODIFY `idGramata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gramatas`
--
ALTER TABLE `gramatas`
  MODIFY `SerijasKods` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gramataszanrs`
--
ALTER TABLE `gramataszanrs`
  MODIFY `idGramatasZanrs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lietotajs`
--
ALTER TABLE `lietotajs`
  MODIFY `idLietotajs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `rezervacija`
--
ALTER TABLE `rezervacija`
  MODIFY `idRezervacija` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `datorarezervacija`
--
ALTER TABLE `datorarezervacija`
  ADD CONSTRAINT `fk_DatoraRezervacija_Dators1` FOREIGN KEY (`idDators`) REFERENCES `dators` (`idDators`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_DatoraRezervacija_Rezervacija1` FOREIGN KEY (`idRezervacija`) REFERENCES `rezervacija` (`idRezervacija`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dators`
--
ALTER TABLE `dators`
  ADD CONSTRAINT `fk_Dators_Biblioteka1` FOREIGN KEY (`idBiblioteka`) REFERENCES `biblioteka` (`idBiblioteka`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gramatas`
--
ALTER TABLE `gramatas`
  ADD CONSTRAINT `fk_Gramatas_Gramata1` FOREIGN KEY (`idGramata`) REFERENCES `gramata` (`idGramata`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gramatasautors`
--
ALTER TABLE `gramatasautors`
  ADD CONSTRAINT `fk_GramatasAutors_Autors1` FOREIGN KEY (`idAutors`) REFERENCES `autors` (`idAutors`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_GramatasAutors_Gramata1` FOREIGN KEY (`idGramata`) REFERENCES `gramata` (`idGramata`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gramatasrezervacija`
--
ALTER TABLE `gramatasrezervacija`
  ADD CONSTRAINT `fk_GramatasRezervacija_Gramatas1` FOREIGN KEY (`SerijasKods`) REFERENCES `gramatas` (`SerijasKods`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_GramatasRezervacija_Rezervacija1` FOREIGN KEY (`idRezervacija`) REFERENCES `rezervacija` (`idRezervacija`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `gramataszanri`
--
ALTER TABLE `gramataszanri`
  ADD CONSTRAINT `fk_GramatasZanri_Gramata1` FOREIGN KEY (`idGramata`) REFERENCES `gramata` (`idGramata`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_GramatasZanri_GramatasZanrs` FOREIGN KEY (`idGramatasZanrs`) REFERENCES `gramataszanrs` (`idGramatasZanrs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rezervacija`
--
ALTER TABLE `rezervacija`
  ADD CONSTRAINT `fk_Rezervacija_Lietotajs1` FOREIGN KEY (`Lietotajs_idLietotajs`) REFERENCES `lietotajs` (`idLietotajs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
