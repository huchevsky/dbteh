<?php
include('head.php');
include('navigation.php');
include('sidenavigation.php');
?>

<div class="container col-lg-9 navbar-default" style="padding-top: 10px;">
    <legend>Grāmatas</legend>
    <div class="row">
        <table class="table" id="table">
            <thead>
            <tr>
                <th>Nosaukums</th>
                <th>Apraksts</th>
                <th>Bibliotēka</th>
                <th>Izdošanas gads</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('config.php');
            $name = $_GET['Name'];
            $author = $_GET['Autors'];
            $biblio = $_GET['Biblio'];

            $sqlAuth = '';
            if (!empty($author)) {
                $sqlAuth = " AND EXISTS(SELECT idGramata FROM gramatasautors WHERE gramatasautors.idAutors = " . $author . ")";
            }

            $sqlBiblio = '';
            if (!empty($author)) {
                $sqlBiblio = " AND gramatas.idBiblioteka = " . $biblio;
            }
            $sql = "SELECT *, gramata.Nosaukums as nos FROM gramatas JOIN gramata ON gramata.idGramata = gramatas.idGramata JOIN biblioteka ON biblioteka.idBiblioteka = gramatas.idBiblioteka
WHERE LOWER(gramata.Nosaukums) LIKE LOWER('%" . $name . "%')" . $sqlAuth . $sqlBiblio;

            echo $sql;

            $sql2 = 'SELECT * FROM gramatasrezervacija JOIN rezervacija 
ON rezervacija.idRezervacija = gramatasrezervacija.idRezervacija WHERE SerijasKods = ';

            $result = mysqli_query($conn, $sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo '<tr>';
                    echo '<td>' . $row["nos"] . '</td>';
                    echo '<td>' . $row["Apraksts"] . '</td>';
                    echo '<td>' . $row["Nosaukums"] . " " . $row["Pilseta"] . ", " . $row["Iela"] . '</td>';
                    echo '<td>' . $row["Izdosanas_gads"] . '</td>';
                    $result2 = mysqli_query($conn, $sql2 . $row["SerijasKods"]);
                    if ($result2->num_rows > 0) {
                        // output data of each row
                        echo '<td>Nav pieejams</td>';
                    } else {
                        echo '<td><a href="book_result_reserve.php?id='. $row["SerijasKods"] .'" class="btn btn-primary">Rezervēt</a></td>';
                    }
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <hr>
    </div>
</div>

<?php
include('bottom.php');
?>
